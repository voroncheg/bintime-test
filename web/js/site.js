$(function() {
    $('.delete_user').on('click', function(){
        var user_id = $(this).data('user-id');
        $.ajax({
            url: '/user/delete-user',
            type: 'POST',
            data: {
                userId: user_id
            },
            success: function () {
                alert('User deleted');
                $.ajax({
                    url: window.location.href,
                    success:function (res) {
                        $('.users_list').html($('.users_list', res).html());
                    }
                })
            },
            error: function () {
                alert('error');
            }
        });
    });

    $('.delete_address').on('click', function(){
        var address_id = $(this).data('address-id')
        $.ajax({
            url: '/user/delete-address',
            type: 'POST',
            data: {
                addressId: address_id
            },
            success: function () {
                alert('Address deleted');
                $.ajax({
                    url: window.location.href,
                    success:function (res) {
                        $('.address_list').html($('.address_list', res).html());
                    }
                })
            },
            error: function () {
                alert('error');
            }
        });
    });


});