<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
        <div class="add_address">
            <div class="form-group field-address-<?=$index?>-post_index required">
                <label class="control-label" for="address-<?=$index?>-post_index">Post Index</label>
                <input type="text" id="address-<?=$index?>-post_index" class="form-control" name="Address[<?=$index?>][post_index]">

                <div class="help-block"></div>
            </div><div class="form-group field-address-<?=$index?>-country required">
                <label class="control-label" for="address-<?=$index?>-country">Country</label>
                <input type="text" id="address-<?=$index?>-country" class="form-control" name="Address[<?=$index?>][country]">

                <div class="help-block"></div>
            </div><div class="form-group field-address-<?=$index?>-city required">
                <label class="control-label" for="address-<?=$index?>-city">City</label>
                <input type="text" id="address-<?=$index?>-city" class="form-control" name="Address[<?=$index?>][city]">

                <div class="help-block"></div>
            </div><div class="form-group field-address-<?=$index?>-street required">
                <label class="control-label" for="address-<?=$index?>-street">Street</label>
                <input type="text" id="address-<?=$index?>-street" class="form-control" name="Address[<?=$index?>][street]">

                <div class="help-block"></div>
            </div><div class="form-group field-address-<?=$index?>-house_number required">
                <label class="control-label" for="address-<?=$index?>-house_number">House Number</label>
                <input type="text" id="address-<?=$index?>-house_number" class="form-control" name="Address[<?=$index?>][house_number]">

                <div class="help-block"></div>
            </div><div class="form-group field-address-<?=$index?>-office required">
                <label class="control-label" for="address-<?=$index?>-office">Office</label>
                <input type="text" id="address-<?=$index?>-office" class="form-control" name="Address[<?=$index?>][office]">

                <div class="help-block"></div>
            </div>                </div>
        </div>

