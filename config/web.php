<?php
    $db = require __DIR__ . '/db.php';

    $config = [
        'id' => 'bintime',
        'basePath' => realpath(__DIR__.'/../'),
        'bootstrap' => ['debug'],
        'modules' => [
            'debug' => [
                'class' => 'yii\debug\Module',
                'allowedIPs' => ['*', '127.0.0.1', '::1']
            ],
        ],
        'components' => [
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false
            ],
            'request' => [
                'enableCookieValidation' => false
            ],
            'db' => $db
        ]
    ];

    return $config;


