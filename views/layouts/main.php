<?php $this->beginPage();

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;use yii\helpers\Html;
?>
<html>
<head>
    <?= Html::csrfMetaTags() ?>
    <title>Bintime test</title>
    <?php AppAsset::register($this); ?>
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>
<?php
    \yii\bootstrap\NavBar::begin([
            'brandLabel' => 'Bintime',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                    'class' => 'navbar-default navbar-fixed-top'
            ]
    ]);
    $items = [
        ['label' => 'AddUser', 'url' => ['/user/adduser']],
        ['label' => 'Users', 'url' => ['/user/users']]
    ];
    echo \yii\bootstrap\Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => $items
    ]);
    \yii\bootstrap\NavBar::end();
    ?>
    <div class="container main_container">
        <?= $content?>
    </div>
<?php AppAsset::register($this); ?>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
