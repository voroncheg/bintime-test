<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<?php if(Yii::$app->session->hasFlash('success')) :?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php if(Yii::$app->session->hasFlash('error')) :?>
    <div class="alert alert-danger" role="alert">
        <?php echo Yii::$app->session->getFlash('error');?>
    </div>
<?php endif; ?>
<div class="panel panel-info">
    <div class="panel-heading">
        <h1>Add address</h1>
    </div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'id' => 'user-add'
        ]) ?>
        <? echo $form->field($model, 'user_id')->hiddenInput(['value' => $_GET['userId']]); ?>
        <?= $form->field($model, 'post_index')?>
        <?= $form->field($model, 'country')?>
        <?= $form->field($model, 'city')?>
        <?= $form->field($model, 'street')?>
        <?= $form->field($model, 'house_number')?>
        <?= $form->field($model, 'office')?>
        <?=Html::submitButton('Add address', ['class' => 'btn btn-success'])?>
        <?php ActiveForm::end() ?>
    </div>
</div>

