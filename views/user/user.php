<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>
<?php if(Yii::$app->session->hasFlash('success')) :?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php if(Yii::$app->session->hasFlash('error')) :?>
    <div class="alert alert-danger" role="alert">
        <?php echo Yii::$app->session->getFlash('error');?>
    </div>
<?php endif; ?>
<div class="panel panel-info">
    <div class="panel-heading">
        <h1>Edit User <?php echo $user->firstname.' '. $user->lastname?></h1>
    </div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin() ?>
        <?= $form->field($model, 'login')?>
        <?= $form->field($model, 'password')->passwordInput()?>
        <?= $form->field($model, 'firstname')?>
        <?= $form->field($model, 'lastname')?>
        <?php $model->gender = $model::UNDEFINEND?>
        <?= $form->field($model, 'gender')
            ->radioList(
                [$model::MALE => 'Male', $model::FEMALE => 'Female', $model::UNDEFINEND => 'Undefined']
            )->label('Gender');
        ?>
        <?php $model->date = date('d-m-Y H:i') ?>
        <?= $form->field($model, 'date')->textInput(['readonly' => true])?>
        <?= $form->field($model, 'email')->input('email')?>
        <?=Html::submitButton('Edit user', ['class' => 'btn btn-success'])?>
        <?php ActiveForm::end() ?>
    </div>
    <div>
        <a href="/user/addresses?userId=<?=$user->id?>">User addresses</a>
    </div>
</div>
