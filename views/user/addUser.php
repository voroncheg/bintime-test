<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\Pjax;
?>
<?php if(Yii::$app->session->hasFlash('success')) :?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php if(Yii::$app->session->hasFlash('error')) :?>
    <div class="alert alert-danger" role="alert">
        <?php echo Yii::$app->session->getFlash('error');?>
    </div>
<?php endif; ?>
<div class="panel panel-info">
        <div class="panel-heading">
            <h1>Add user</h1>
        </div>
        <div class="panel-body">
                <?php $form = ActiveForm::begin([
                        'id' => 'user-add'
                ]) ?>
                    <?= $form->field($model, 'login')?>
                    <?= $form->field($model, 'password')->passwordInput()?>
                    <?= $form->field($model, 'firstname')?>
                    <?= $form->field($model, 'lastname')?>
                    <?php $model->gender = $model::UNDEFINEND?>
                    <?= $form->field($model, 'gender')
                    ->radioList(
                            [$model::MALE => 'Male', $model::FEMALE => 'Female', $model::UNDEFINEND => 'Undefined']
                    )->label('Gender');
                   ?>
                   <?php $model->date = date('d-m-Y H:i') ?>
                    <?= $form->field($model, 'date')->textInput(['readonly' => true])?>
                    <?= $form->field($model, 'email')->input('email')?>
            <p class="add_address_head">Add address</p>

            <div class="addresses">
                <div class="add_address">
                    <?php    foreach ($addressFields as $field):
                        echo   $form->field($address, "[0]$field");
                    endforeach; ?>
                </div>
            </div>
            <p class="add_more_address">
            <?php
                echo Html::a('Добавить адрес', ['', 'index' => $index], [
                    'class' => 'btn btn-primary js-add-address',
                ]);
            ?>
            </p>

                    <?=Html::submitButton('Add user', ['class' => 'btn btn-success'])?>
                <?php ActiveForm::end() ?>
        </div>
</div>
<?php
$modalAuthReg = <<< JS

$('.js-add-address').on('click', function(){
    var index = $('.add_address').length;
        $.ajax({
            url: "/user/dublicate-address?index=" + index,

            success: function(data) {
                $('.addresses').append(data)
            },
            error: function () {
                alert('error');
            }
        });
        return false;
    });
JS;


$this->registerJs($modalAuthReg, \yii\web\View::POS_LOAD);
