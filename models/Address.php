<?php


namespace app\models;


use yii\db\ActiveRecord;
use app\models\Users;

class Address extends ActiveRecord
{
    public static function tableName(){
        return 'addresses';
    }

    public function rules()
    {

        return [
            [['post_index', 'country', 'city','street','house_number'], 'required'],
            ['post_index', 'match', 'pattern' => '/[0-9]/', 'message' => 'This field should be filling numbers'],
            ['country', 'string', 'max' => 2],
            ['country', 'match', 'pattern' => '/[A-Z]{2}/', 'message' => 'This field should be filling Uppercase'],
            ['office','default', 'value' => null]
        ];

    }

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}