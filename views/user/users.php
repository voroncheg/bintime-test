<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\grid\GridView;
?>
<h1>Users</h1>
<ul class="users_list">
    <?php foreach ($models as $user): ?>
        <li>
            <?php
                echo $user->firstname.' '.$user->lastname;
            ?>
            <p>
                <a href="<?php echo '/user/user?userId='.$user->id ?>" class="btn">Edit user</a>
                <a href="#" class="delete_user" data-user-id="<?=$user->id?>">Delete user</a>

            </p>

        </li>
    <?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>
