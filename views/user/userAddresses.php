<?php
    use yii\widgets\LinkPager;

?>

<h1>User addresses</h1>
<ul class="address_list">
    <?php foreach ($addresses as $address): ?>
        <li>
            <?php
            echo $address->country.' '.$address->city;
            ?>
            <p>
                <a href="<?php echo '/user/address?addressId='.$address->id ?>" class="btn">Edit address</a>
                <a href="#" class="delete_address" data-address-id="<?=$address->id?>">Delete address</a>

            </p>

        </li>
    <?php endforeach; ?>
</ul>

<div class="add_new_address">
    <a href="/user/add-address?userId=<?=$_GET['userId']?>" class="add_new_address">Add new address</a>
</div>
