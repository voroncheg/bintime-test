<?php

namespace app\models;
use yii\db\ActiveRecord;
use app\models\Address;

class Users extends ActiveRecord
{
    public $address;

    const MALE = 1;
    const FEMALE = 2;
    const UNDEFINEND = 0;

    public static function tableName(){
        return 'users';
    }

    public function rules(){
        return [
            [['login', 'password', 'firstname','lastname','gender','date', 'email'], 'required'],
            [['login', 'email'], 'unique', 'message' => 'The field should be unique'],
            ['login', 'string', 'min' => 4],
            ['password', 'string', 'min' => 6],
            [['firstname','lastname'], 'match', 'pattern' => '/[A-Z][a-zA-Z]*\s*/', 'message' => 'This field should be filling Capitalize'],
            ['email','email']
        ];
    }

    public function getAdress()
    {
        return $this->hasMany(Address::className(), ['user_id' => 'id']);
    }
}