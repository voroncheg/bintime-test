<?php


namespace app\controllers;


use app\models\Address;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Controller;
use app\models\Users;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\web\User;
use yii\widgets\ActiveForm;

class UserController extends Controller
{
    public function actionAdduser(){
          $model = new Users();
          $address = new Address();

          $addressFields = $this->getAddressFields();

          if($model->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post())){
                $post_data = Yii::$app->request->post();
              $transaction = Yii::$app->db->beginTransaction();
              try  {
                  if ($model->save()) {
                      foreach ($post_data['Address'] as $post_val){
                          Yii::$app->db->createCommand()->insert('addresses', [
                              'user_id' => $model->id,
                              'post_index' => $post_val['post_index'],
                              'country' => $post_val['country'],
                              'city' => $post_val['city'],
                              'street' => $post_val['street'],
                              'house_number' => $post_val['house_number'],
                              'office' => $post_val['office'],
                          ])->execute();
                      }
                      $transaction->commit();
                      Yii::$app->session->setFlash('success', 'User added');
                      return $this->refresh();
                  } else {
                      $transaction->rollBack();
                      Yii::$app->session->setFlash('error', 'Data error');
                  }
              } catch (Exception $e) {
                  $transaction->rollBack();
                  Yii::$app->session->setFlash('error', 'Data error2222');
              }
          }

        return $this->render('adduser', [
            'model' => $model,
            'address' => $address,
            'addressFields' => $addressFields,
            'index' => $index
        ]);
    }

    private function getAddressFields(){
        $address = new Address();
        $attrArr = $address->attributes;
        unset($attrArr['id']);
        unset($attrArr['user_id']);
        $attrArr = array_keys($attrArr);
        return $attrArr;
    }

    public function actionDeleteUser(){
        $userId = (int)\Yii::$app->request->post('userId');
        if($userId){
            $user = Users::findOne($userId);
            if(!empty($user)){
                if($user->delete()){
                    return true;
                }
            }
        }
        return false;
    }

    public function actionDeleteAddress(){
        $addressId = (int)\Yii::$app->request->post('addressId');
        if($addressId){
            $address = Address::findOne($addressId);
            if(!empty($address)){
                if($address->delete()){
                    return true;
                }
            }
        }
        return false;
    }

    public function actionUsers(){

        $users = Users::find()->all();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => count($users),
        ]);


        return $this->render('users', [
            'models' => $users,
            'pagination' => $pagination
        ]);
    }

    public function actionUser($userId){

        $user = Users::findOne($userId);
        $model = new Users();
        $model->login = $user->login;
        $model->password = $user->password;
        $model->firstname = $user->firstname;
        $model->lastname = $user->lastname;
        $model->gender = $user->gender;
        $model->date = $user->date;
        $model->email = $user->email;

        if($model->load(\Yii::$app->request->post())){

            if($model->save()){
                \Yii::$app->session->setFlash('success', 'User edited');
                return $this->refresh();
            }else{
                \Yii::$app->session->setFlash('error', 'Data error');
            }

        }

        return $this->render('user', ['user'=>$user, 'model' => $model]);
    }

    public function actionDublicateAddress($index){
        $address = new Address();
        $model = new Users();
        $addressFields = $this->getAddressFields();

        return $this->renderAjax('_addfields',[
            'model' => $model,
            'address' => $address,
            'addressFields' => $addressFields,
            'index' => $index
        ]);
    }

    public function actionAddresses($userId){
        $addresses = Address::find()->where(['user_id' => $userId])->all();

        return $this->render('useraddresses',[
            'addresses' => $addresses
        ]);
    }

    public function actionAddress($addressId){

        $address = Address::findOne($addressId);
        $model = new Address();
        $model->post_index = $address->post_index;
        $model->country = $address->country;
        $model->city = $address->city;
        $model->street = $address->street;
        $model->house_number = $address->house_number;
        $model->office = $address->office;

        if($model->load(\Yii::$app->request->post())){

            if($model->save()){
                \Yii::$app->session->setFlash('success', 'Address edited');
                return $this->refresh();
            }else{
                \Yii::$app->session->setFlash('error', 'Data error');
            }

        }

        return $this->render('address', ['address'=>$address, 'model' => $model]);
    }

    public function actionAddAddress($userId){

        $address = new Address();

        if($address->load(\Yii::$app->request->post())){
            $address->user_id = $_GET['userId'];

            if($address->save()){
                \Yii::$app->session->setFlash('success', 'Address added');
                return $this->refresh();
            }else{
                \Yii::$app->session->setFlash('error', 'Data error');
            }

        }

        return $this->render('addaddress',[
            'model' => $address
        ]);
    }


}